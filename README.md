# message_sendler

Для запуска необходимо: 
Установить все зависимости из файла requirenments.txt в виртуальное окружение.
Установить в окружении переменные: "SEND_TOKEN", "SEND_API_URL", для интеграции с внешним сервисом отправки сообщений.
Запустить файл db/db_maker.py для создания БД.
С помощью uvicorn запустить сервер (uvicorn main:app --reload)

Проблема: при отправке запросов на внешний API приходит ответ с кодом 400.
Для корректной работы сервиса с внешним API необходимо обернуть запрос в блок try-except, а так же в try сделать обработку кодов ответа, при коде 200 вносить изменения в БД.

При создании рассылки (mailing_list), создаются сообщения, имеющие статусы "no_sent", далее при отправке в случае кода ответа от внешнего API 200 изменяются статус отправки и время отправки.

При изменении фильтров для отправки из таблици messages удаляются сообщения измененного фильтра и подгружаются новые.

При создании рассылки создается задача, ожидающая старта рассылки.
Если при изменении параметров рассылки время старта рассылки изменилось, создается новая задача по рассылки, ожидающая своего старта.
При старте рассылки в запланированное время, проверяется, не изменилось ли время (если изменилось, отправка не совершается, там как для нее уже есть другая задача рассылки).
